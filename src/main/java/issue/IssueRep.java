package issue;


import org.springframework.data.jpa.repository.JpaRepository;


public interface IssueRep  extends JpaRepository<Issue, String>  {

}
