package issue;

import java.time.LocalDateTime;
import java.util.UUID;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.Id;

@DynamicUpdate
public class Issue {
	
	@Id
	UUID issue_guid;
	
	@Type(type = "text")
	private String issue_desc;
	
	public Issue() {
		this.issue_guid = UUID.randomUUID();
	}

	private LocalDateTime date_creation;
}
